const { employeeIdList, groupDataOnCompanies, dataForCompany, dataWithoutId2, sortDataBasedOnCompanyNameOrId, swapdata, dataToFindEvenAndAddBrthday } = require('../employee')
const data = require("../data.json")

let dataObj = data.employees

employeeIdList(dataObj, (err, response) => {
    if (err) {
        console.log(err)
    }
    groupDataOnCompanies(dataObj, (err, response) => {
        if (err) {
            console.log(err.message)
        }
        dataForCompany(dataObj, (err, response) => {
            if (err) {
                console.log(err.message)
            }
            dataWithoutId2(dataObj, (err, response) => {
                if (err) {
                    console.log(err.message)
                }
                sortDataBasedOnCompanyNameOrId(dataObj, (err, response) => {
                    if (err) {
                        console.log(err.message)
                    } swapdata(dataObj, (err, response) => {
                        if (err) {
                            console.log(err.message)
                        }
                        dataToFindEvenAndAddBrthday(dataObj, (err, response) => {
                            if (err) {
                                console.log(err.message)
                            } else {
                                console.log(response)
                            }
                        })
                    })
                })
            })
        })
    })
})