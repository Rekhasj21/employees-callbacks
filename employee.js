const fs = require('fs')

function employeeIdList(dataObj, callback) {
    const id = [2, 13, 23]
    const employeeIdList = dataObj.filter(employee => id.includes(employee.id))
    fs.writeFile("jsonFile/employee1.json", JSON.stringify(employeeIdList), (err, response) => {
        if (err) {
            callback(err, null);
            return;
        } else {
            callback(null, response)
        }
    })
}
function groupDataOnCompanies(dataObj, callback) {
    const groupDataOnCompanies = dataObj.reduce((acc, dataList) => {
        if (acc[dataList.company]) {
            acc[dataList.company] = dataList
        } else {
            acc[dataList.company] = []
        }
        return acc
    }, {})
    fs.writeFile('jsonFile/employee2.json', JSON.stringify(groupDataOnCompanies), (err, response) => {
        if (err) {
            callback(err, null)
            return
        } else {
            callback(null, "Success")
        }
    })
}
function dataForCompany(dataObj, callback) {
    const dataForCompany = dataObj.filter(({ company }) => company === "Powerpuff Brigade")
    fs.writeFile('jsonFile/employee3.json', JSON.stringify(dataForCompany), (err) => {
        if (err) {
            callback(err, null)
            return
        } else {
            callback(null, "Success")
        }
    })
}
function dataWithoutId2(dataObj, callback) {
    const dataWithoutId2 = dataObj.filter(({ id }) => id !== 2)
    fs.writeFile('jsonFile/employee4.json', JSON.stringify(dataWithoutId2), (err) => {
        if (err) {
            callback(err, null)
            return
        } else {
            callback(null, "Success")
        }
    })
}
function sortDataBasedOnCompanyNameOrId(dataObj, callback) {
    const dataForSort = dataObj
    const sortDataBasedOnCompanyNameOrId = dataForSort.sort((a, b) => {
        const aCompany = a.company.trim()
        const bCompany = b.company.trim()

        if (aCompany < bCompany) {
            return -1
        }
        if (aCompany > bCompany) {
            return 1
        }
        if (a.id < b.id) {
            return -1
        }
        if (a.id > b.id) {
            return 1
        }
        return 0
    })
    fs.writeFile('jsonFile/employee5.json', JSON.stringify(sortDataBasedOnCompanyNameOrId), (err) => {
        if (err) {
            callback(err, null)
            return
        } else {
            callback(null, "Success")
        }
    })
}
function swapdata(dataObj, callback) {
    const swapdata = dataObj
    const index93 = swapdata.findIndex(record => record.id === 93);
    const index92 = swapdata.findIndex(record => record.id === 92);

    [swapdata[index92], swapdata[index93]] = [swapdata[index93], swapdata[index92]];

    fs.writeFile('jsonFile/employee6.json', JSON.stringify(swapdata), (err) => {
        if (err) {
            callback(err, null)
            return
        } else {
            callback(null, "Success")
        }
    })
}
function dataToFindEvenAndAddBrthday(dataObj, callback) {
    const dataToFindEvenAndAddBrthday = dataObj
    let today = new Date();
    const currentDate = today.getFullYear() + "-" + today.getMonth() + 1 + "-" + today.getDate();
    const updatedDataAfterAddingBirthDayDate = dataToFindEvenAndAddBrthday.map(employee => {
        if (employee.id % 2 === 0) {
            return { ...employee, birthday: currentDate }
        } else {
            return employee
        }
    })
    fs.writeFile('jsonFile/employee7.json', JSON.stringify(updatedDataAfterAddingBirthDayDate), (err) => {
        if (err) {
            callback(err, null)
            return
        } else {
            callback(null, "Success")
        }
    })
}
module.exports = { employeeIdList, groupDataOnCompanies, dataForCompany, dataWithoutId2, sortDataBasedOnCompanyNameOrId, swapdata, dataToFindEvenAndAddBrthday }